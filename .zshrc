zmodload zsh/terminfo

export TERM=screen-256color
export EDITOR=vim

source ~/.xprofile

#bindkey -v

bindkey '^P' up-history
bindkey '^N' down-history
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word
bindkey '^r' history-incremental-search-backward

#function zle-line-init zle-keymap-select {
#    VIM_PROMPT="%{$fg_bold[black]%} NORMAL %{$reset_color%}"
#    RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/ /} %~   "
#    zle reset-prompt
#}

#zle -N zle-line-init
#zle -N zle-keymap-select
export KEYTIMEOUT=1

export HISTFILE=~/.zhistory
export HISTSIZE=256
export SAVEHIST=256
setopt inc_append_history
setopt extended_history
#setopt share_history

alias clock='tty-clock -c -C4 -Dr'
alias ls='ls --color=auto'
alias la='ls --color=auto -a'
alias testnet='ping 8.8.8.8'
alias mpvnv='mpv --no-video'
alias mpvcp='mpv $(xsel -ob)'
alias mpvcpnv='mpv --no-video $(xsel -ob)'

function attach {
    test -z "$1" && {
        tmux attach
    } || {
        tmux attach -t "$1"
    }
}

autoload -U promptinit && promptinit
autoload -U colors && colors

PROMPT=" %{$fg[white]%}────%{$reset_color%}  "
RPROMPT="%~   "
