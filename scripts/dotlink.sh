#!/bin/zsh

DOTFILES="$HOME/dotfiles"

I3=".i3"
XRES=".xres"
VIM=".vim"
WEECHAT=".weechat"
CONFIG=".config"
NCMPCPP=".ncmpcpp"
MPD=".mpd"

function link {
    if [ -z $1 ]; then
        echo "missing arguments to link function."
        echo "usage: link /path/to/file"
        exit 1
    fi
    FILE=$1
    rm -rf $HOME/$FILE
    ln -s $DOTFILES/$FILE $HOME/$FILE
}

function folder {
    if [ -z $1 ]; then
        echo "missing arguments to folder function"
        echo "usage: folder /path/to/folder"
        exit 1
    fi
    FOLDER=$1
    ln -s $HOME/media/$FOLDER $HOME/$FOLDER
}

if [ -n $1 ] && [ "$1" != "home"]; then
    link $1
    exit
fi

# i3 files
link $I3

# xres files
link $XRES

# vim files
link $VIM

# weechat files
link $WEECHAT

# ncmpcpp files
link $NCMPCPP

# mpd files
link $MPD

# other stuff
link ".compton.conf"
link ".env"
link ".bin"
link ".urxvt"
link ".xprofile"
link ".fonts"
link ".zshrc"
link ".Xmodmap"
link ".xinitrc"
link ".vimrc"
link ".asoundrc"
link ".rtorrent.rc"
link ".volume_control"
link ".gitconfig"
link ".electrum"
link ".ssh"
link ".themes"
link ".mozilla"
link ".mpc-play-pause"
link ".tmux.conf"
link "scripts"
link "startpage"

mkdir $HOME/.config
mkdir $HOME/.mpd
mkdir $HOME/.irssi

link ".irssi/pastie.theme"
link ".irssi/config"

link ".config/bar"
link ".config/sxhkd"
link ".mpd/mpd.conf"

# setup home folders
if [ "$1" = "home" ]; then
    folder documents
    mkdir -p $HOME/downloads
    folder music
    folder images
    folder programming
    mkdir -p $HOME/watch

    # generate some files
    ./scripts/update_i3
fi


