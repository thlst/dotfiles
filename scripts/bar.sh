#!/bin/zsh

if [ -z $1 ] || [ -z $2 ]; then
    echo "missing arguments"
    echo "usage: bar command max [position]"
    exit
fi

if [ -z $3 ]; then
    POS=0
else
    POS=$3
fi

CMD=$1
MAX=$2

PW=150
PH=8
PX=$((15 + ($PW+10) * $POS))
PY=20

FONT="boxxy"

WEIGHT=$(($PW/4))

calculate() {
    CUR=$(eval $CMD)
    PERC=$((($WEIGHT*$CUR)/$MAX))
    for i in {1..$PERC}
    do
        bar+=" "
    done
    echo "$bar"
}

# TODO Eval it continuously
while :; do
    sleep 2
    echo "%{R}$(calculate)%{R}"
done |\
    lemonbar -g ${PW}x${PH}+${PX}+${PY}  -B "#252525" -F "$FG" -p -d -f "$FONT"

