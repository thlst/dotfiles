set $fg #eddcd3
set $bg #000000
set $lbg #030507

#set $float exec --no-startup-id ~/.config/shadow_work_around.sh, floating toggle

set $mod Mod4

# window_border pixel 4

gaps inner 5 #15
gaps outer 0 # 50

# Layout colors 	      bord 	   bg 	    text     indicator (split)
client.focused 		      $fg      $fg      $fg      $fg
client.focused_inactive   $lbg     $lbg     $lbg     $lbg
client.unfocused 	      $lbg     $lbg     $lbg     $lbg
client.urgent 		      $fg      $fg      $fg      $fg
client.background 	      $lbg

### Borders ###
new_window pixel 3
new_float pixel 2

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Unset mouse focus
focus_follows_mouse no

# Opens the default browser
bindsym $mod+b exec grab_surf
bindsym $mod+Home exec surf ~/startpage/homepage.html

# Pops a bar
bindsym $mod+End exec zsh ~/.config/bar/pop

# Date and time
bindsym $mod+F12 exec notify_bar alert 016 "$(date)" 20

# battery
bindsym $mod+F11 exec notify_bar alert 238 "$(battery)"

# start a terminal
#bindsym $mod+Return exec i3-sensible-terminal
bindsym $mod+Return exec --no-startup-id urxvt

#for_window [class="URxvt" instance="floating_urxvt$"] floating toggle

# kill focused window
bindsym $mod+Shift+q kill

# start rofi (a program launcher)
bindsym $mod+d exec rofi -show run -font "terminus 10"

bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# split in horizontal orientation
bindsym $mod+g split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space $float

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

set $ws1 work
set $ws2 web
set $ws3 chat
set $ws4 fourth
set $ws5 fifth
set $ws6 sixth
set $ws7 seventh
set $ws8 eighth
set $ws9 ninth
set $ws10 tenth

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# Multimedia Keys
# increase volume
bindsym XF86AudioRaiseVolume exec bash ~/.volume_control 5%+
# decrease volume
bindsym XF86AudioLowerVolume exec bash ~/.volume_control 5%-
# mute volume
bindsym XF86AudioMute exec bash ~/.volume_control mute
# pause / play / next / previous
bindsym XF86AudioPlay exec zsh ~/.mpc-play-pause
bindsym XF86AudioNext exec mpc next
bindsym XF86AudioPrev exec mpc prev

# Prints my screen
bindsym Print exec scrot -e 'mv $f ~/images/screenshots'

# Prints my screen by selecting a rectangle
bindsym $mod+Print exec scrotup

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'pkill zsh & pkill lemonbar & i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
#bar {
#	position top
#	font pango:Source Sans Pro-10 8
#	status_command zsh ~/.conky-i3bar

#	colors {
#		statusline		#5c616c
#		background		$FOCUS

		# class			border		backgrd		text

#		focused_workspace	$5c616c 	$UNFOCUS	#5f636f
#		active_workspace	#7D7D7D 	$FOCUS		#5c616c
#		inactive_workspace	$UNFOCUS 	$UNFOCUS	#5c616c
#		urgent_workspace	#000000 	#000000		#2323CF
#	}
#}

#exec --no-startup-id compton

#exec --no-startup-id conky -c $HOME/.config/bar/conky
#exec --no-startup-id zsh $HOME/.config/bar/bar
