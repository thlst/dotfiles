set nocompatible              " be iMproved, required
filetype off                  " required


" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Rust plugin
Plugin 'rust-lang/rust.vim'

Plugin 'Junza/Spink'

Plugin 'whatyouhide/vim-gotham'

Plugin 'vim-airline/vim-airline'

Plugin 'vim-airline/vim-airline-themes'

"Plugin 'airblade/vim-gitgutter'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal

"set background=dark
colorscheme monochrome

syntax on
"let g:rustfmt_autosave = 1

"let g:airline_right_alt_sep = ''
"let g:airline_right_sep = ''
"let g:airline_left_alt_sep= ''
"let g:airline_left_sep = ''

let g:airline_powerline_fonts = 0
"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#left_sep = ' '
"let g:airline#extensions#tabline#left_alt_sep = '░'
" Disable powerline arrows and setting blank seperators creates a rectangular box
let g:airline_left_sep = '█▓░'
let g:airline_right_sep = '░▓█'

"let g:airline_theme = "zenburn"
"let g:airline_theme = "distinguished"
let g:airline_theme = "term"

set laststatus=2
set number

" Intuitive backspacing.
set backspace=indent,eol,start

set ruler

" map commands
map <F4> <ESC>:vs<CR><ESC> :execute "lvimgrep /" . expand("<cword>") . "./**"<CR><ESC>:lw<CR>
map <F11> :!xsel -ib<CR>
map <F12> :r !xsel -ob<CR>

noremap H ^
noremap L $

